import React from "react";
import g from "glamorous";
import Link from 'gatsby-link';

import { rhythm } from '../utils/typography';

export default ({ data }) => {
  console.log(data);
  return (
    <div>
      <h1>Amazing Pandas Eating Things</h1>
      <div>
        <img src="https://2.bp.blogspot.com/-BMP2l6Hwvp4/TiAxeGx4CTI/AAAAAAAAD_M/XlC_mY3SoEw/s1600/panda-group-eating-bamboo.jpg"
             alt="Group of pandas eating bamboo" />
      </div>
      <div>
        <h4>{data.allMarkdownRemark.totalCount} Posts</h4>
          {data.allMarkdownRemark.edges.map(({ node }) => (
            <div key={node.id}>
              <Link to={node.fields.slug} css={{ color: `inherit`}}>
                <g.H3 marginBottom={rhythm(1 / 4)}>
                  {node.frontmatter.title}{" "}
                  <g.Span color="#BBB">— {node.frontmatter.date}</g.Span>
                </g.H3>
              </Link>
              <p>{node.excerpt}</p>
            </div>
          ))}
      </div>
    </div>
  )
};

export const query = graphql`
query IndexQuery {
  allMarkdownRemark(sort: {fields: [frontmatter___date], order: DESC}) {
    totalCount
    edges {
      node {
        frontmatter {
          title
          date(formatString: "MMMM DD, YYYY")
        }
        fields {
          slug
        }
        excerpt
        timeToRead
        html
      }
    }
  }
}
`
